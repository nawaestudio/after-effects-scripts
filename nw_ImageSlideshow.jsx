// CREADOR DE SECUENCIAS DE IMÁGENES

var a = app.project;
var w = 1920;
var h = 1080;
var pix_ratio = 1;
var fps = 25;
var frame_duration = 1 / fps;

// Duración de etiquetas
var nolabel = 2; // Gente normal
var green = nolabel * 1.5; // Importante o buena
var red = nolabel / 2; // Paisajes
var orange = nolabel * 3; // Foto especial

// Variavles de efectos
var scale_mult = 1.05;
var easeIn = new KeyframeEase(1, 100);
var easeOut = new KeyframeEase(1, 100);
var vertical_photo_log = [];

function getRndInteger(min, max) {
  return Math.floor(Math.random() * (max - min + 1) ) + min;
}

app.beginUndoGroup("IMAGE SEQUENCER");

if (a.selection == 0) {
	alert("Debes seleccionar una carpeta")
}

else {

	var master_comp_name = prompt("Nombre de la secuencia");

	var temp_dur = 1;
	var trans_frames = 25; // en frames
	var trans_dur = trans_frames * frame_duration;
	var total_dur = 0;
	var created_comps = [];

	for (i = 0; i < a.selection.length; i++) {
		var folder = a.selection[i]
		var name = folder.name;
		var comp = a.items.addComp(name, w, h, pix_ratio, temp_dur, fps);
		var curTime = 0;

		for (j = 1; j <= folder.numItems; j++) {
			src_item = folder.item(j);
			var src_width = src_item.width;
			var src_height = src_item.height;
			var label = src_item.label;

			if (src_height > src_width) {
				var isVertical = true;
			}

			else {
				var isVertical = false;
			}

			switch (true) {
				case (label == 1): // Etiqueta roja
					var dur = red;
					break;

				case (label == 5): // Sin etiqueta
					var dur = nolabel;
					break;

				case (label == 9): // Etiqueta verde
					var dur = green;
					break;

				case (label == 11): // Etiqueta verde
					var dur = orange;
					break;
			}

			if (isVertical == true) {
				var resize = h * 100 / src_height;
				var resize_bg = w * 100 / src_width;
				vertical_photo_log.push(folder.name + ": " + src_item.name);

				var layer_bg = comp.layers.add(src_item, dur + trans_dur);
				layer_bg.startTime = curTime;
				layer_bg.scale.setValue([resize_bg,resize_bg]);
			}
			else if (src_width / src_height > w / h) {
				var resize = h * 100 / src_height;
			}
			else {
				var resize = w * 100 / src_width;
			}

			var layer = comp.layers.add(src_item, dur + trans_dur);
			layer.startTime = curTime;

			layer.scale.setValue([resize, resize]);
			var new_w = src_width * (resize / 100);
			var new_h = src_height * (resize / 100);

			var extra_w = new_w - w; // Anchura extra respecto a la comp
			var extra_h = new_h - h; // Altura extra respecto a la comp

			// RANDOM EFECT

			var effect = getRndInteger(0, 5);

			if (effect == 0) { // Barrido desde arriba
				if (isVertical == true) {
					layer_bg.position.setValuesAtTimes([layer.inPoint, layer.inPoint + trans_dur], [[comp.width / 2, -src_height / 2], [comp.width / 2, comp.height / 2]])
					layer_bg.position.setTemporalEaseAtKey(2, [easeIn])
					var gaussBlur = layer_bg.property("ADBE Effect Parade").addProperty("ADBE Gaussian Blur 2");
					gaussBlur.property("ADBE Gaussian Blur 2-0001").setValue(150);
					gaussBlur.property("ADBE Gaussian Blur 2-0003").setValue(1);
				}
				layer.position.setValuesAtTimes([layer.inPoint, layer.inPoint + trans_dur], [[comp.width / 2, -src_height / 2], [comp.width / 2, comp.height / 2]])
				layer.position.setTemporalEaseAtKey(2, [easeIn])
			}
			else if (effect == 1) { // Barrido desde la derecha
				if (isVertical == true) {
					layer_bg.position.setValuesAtTimes([layer.inPoint, layer.inPoint + trans_dur], [[comp.width * 1.5, comp.height / 2], [comp.width / 2, comp.height / 2]])
					layer_bg.position.setTemporalEaseAtKey(2, [easeIn])
					var gaussBlur = layer_bg.property("ADBE Effect Parade").addProperty("ADBE Gaussian Blur 2");
					gaussBlur.property("ADBE Gaussian Blur 2-0001").setValue(150);
					gaussBlur.property("ADBE Gaussian Blur 2-0003").setValue(1);
				}
				layer.position.setValuesAtTimes([layer.inPoint, layer.inPoint + trans_dur], [[comp.width * 1.5, comp.height / 2], [comp.width / 2, comp.height / 2]])
				layer.position.setTemporalEaseAtKey(2, [easeIn])
			}
			else if (effect == 2) { // Barrido desde abajo
				if (isVertical == true) {
					layer_bg.position.setValuesAtTimes([layer.inPoint, layer.inPoint + trans_dur], [[comp.width / 2, comp.height + src_height / 2], [comp.width / 2, comp.height / 2]])
					layer_bg.position.setTemporalEaseAtKey(2, [easeIn])
					var gaussBlur = layer_bg.property("ADBE Effect Parade").addProperty("ADBE Gaussian Blur 2");
					gaussBlur.property("ADBE Gaussian Blur 2-0001").setValue(150);
					gaussBlur.property("ADBE Gaussian Blur 2-0003").setValue(1);
				}
				layer.position.setValuesAtTimes([layer.inPoint, layer.inPoint + trans_dur], [[comp.width / 2, comp.height + src_height / 2], [comp.width / 2, comp.height / 2]])
				layer.position.setTemporalEaseAtKey(2, [easeIn])
			}
			else if (effect == 3) { // Barrido desde la izquierda
				if (isVertical == true) {
					layer_bg.position.setValuesAtTimes([layer.inPoint, layer.inPoint + trans_dur], [[-comp.width * 1.5, comp.height / 2], [comp.width / 2, comp.height / 2]])
					layer_bg.position.setTemporalEaseAtKey(2, [easeIn])
					var gaussBlur = layer_bg.property("ADBE Effect Parade").addProperty("ADBE Gaussian Blur 2");
					gaussBlur.property("ADBE Gaussian Blur 2-0001").setValue(150);
					gaussBlur.property("ADBE Gaussian Blur 2-0003").setValue(1);
				}
				layer.position.setValuesAtTimes([layer.inPoint, layer.inPoint + trans_dur], [[-comp.width * 1.5, comp.height / 2], [comp.width / 2, comp.height / 2]])
				layer.position.setTemporalEaseAtKey(2, [easeIn])
			}
			else if (effect == 4) { // Zoom in
				if (isVertical == true) {
					layer_bg.opacity.setValuesAtTimes([layer.inPoint, layer.inPoint + trans_dur / 2], [0, 100]);
					var gaussBlur = layer_bg.property("ADBE Effect Parade").addProperty("ADBE Gaussian Blur 2");
					gaussBlur.property("ADBE Gaussian Blur 2-0001").setValue(150);
					gaussBlur.property("ADBE Gaussian Blur 2-0003").setValue(1);
				}
				layer.opacity.setValuesAtTimes([layer.inPoint, layer.inPoint + trans_dur / 2], [0, 100]);
				layer.scale.setValuesAtTimes([layer.inPoint, layer.outPoint], [[resize, resize], [resize * scale_mult, resize * scale_mult]]);
			}
			else if (effect == 5) { // Zoom out
				if (isVertical == true) {
					layer_bg.opacity.setValuesAtTimes([layer.inPoint, layer.inPoint + trans_dur / 2], [0, 100]);
					var gaussBlur = layer_bg.property("ADBE Effect Parade").addProperty("ADBE Gaussian Blur 2");
					gaussBlur.property("ADBE Gaussian Blur 2-0001").setValue(150);
					gaussBlur.property("ADBE Gaussian Blur 2-0003").setValue(1);
				}
				layer.opacity.setValuesAtTimes([layer.inPoint, layer.inPoint + trans_dur / 2], [0, 100]);
				layer.scale.setValuesAtTimes([layer.inPoint, layer.outPoint], [[resize * scale_mult, resize * scale_mult], [resize, resize]]);
			}

			layer.motionBlur = true;

			curTime = layer.outPoint - trans_dur;
		}

		comp.duration = curTime + trans_dur;
		comp.motionBlur = true;
		// comp.openInViewer();
		total_dur += comp.duration;
		created_comps.push(comp);
	}

	var master_comp = a.items.addComp(master_comp_name, w, h, pix_ratio, total_dur - trans_dur * a.selection.length, fps);
	master_comp.openInViewer();

	curTime = 0;
	for (k = 0; k < created_comps.length; k++) {
		var comp_layer = master_comp.layers.add(created_comps[k]);
		comp_layer.startTime = curTime;
		curTime += created_comps[k].duration - trans_dur;
	}

	alert(vertical_photo_log.join("\r\n"));
}

app.endUndoGroup();

