// 360 GRADOS 2019 - DROP SHADOW
// Guillermo Sevillano 2019

// RGB & OPACITY NORMALIZE

app.beginUndoGroup("Sombra 360");

var color = [28,192,170];
var norm_color = color / 255;
var opacity = 50;
var norm_opacity = opacity * 2.55;

// Variables

var sel = app.project.activeItem.selectedLayers;
var capa = sel[0];

var dir = 135;
var dist = 7;

var fx_shadow = capa.property("Effects").addProperty("ADBE Drop Shadow");

// Añade las expresiones para los controles anteriores

var shadow_col = capa.property("ADBE Effect Parade").property("ADBE Drop Shadow").property("ADBE Drop Shadow-0001");
shadow_col.setValue(norm_color);
var shadow_opacity = capa.property("ADBE Effect Parade").property("ADBE Drop Shadow").property("ADBE Drop Shadow-0002");
shadow_opacity.setValue(norm_opacity);
var shadow_dir = capa.property("ADBE Effect Parade").property("ADBE Drop Shadow").property("ADBE Drop Shadow-0003");
shadow_dir.setValue(dir);
var shadow_dist = capa.property("ADBE Effect Parade").property("ADBE Drop Shadow").property("ADBE Drop Shadow-0004");
shadow_dist.setValue(dist);

app.endUndoGroup();

