// CREACIÓN DE FORMAS
// Guillermo Sevillano 2019

app.beginUndoGroup("Rectángulo");

var comp = app.project.activeItem;

var color = [0.898,0.898,0.898]
var stroke = 6;
var easeIn = new KeyframeEase(0.5, 100);
var easeOut = new KeyframeEase(0.5, 100);

var w = comp.width;
var h = comp.height;

var dir = 3; // Rango 1-3 Para la dirección de la forma. (1, 2 para CW y 3 para CCW)

var rect_forma = comp.layers.addShape();
rect_forma.name = "RECT";
rect_forma.inPoint = comp.time;
rect_forma.motionBlur = true;

// AÑADIR CONTROLES DE EXPRESION

var ancho_prop = rect_forma.property("Effects").addProperty("ADBE Slider Control");
ancho_prop.name = "Ancho";
var alto_prop = rect_forma.property("Effects").addProperty("ADBE Slider Control");
alto_prop.name = "Alto";
var grosor_prop = rect_forma.property("Effects").addProperty("ADBE Slider Control");
grosor_prop.name = "Grosor Trazo";
var color_prop = rect_forma.property("Effects").addProperty("ADBE Color Control");
color_prop.name = "Color";

// AÑADIR VALUES
var color_ctrl = rect_forma.property("ADBE Effect Parade").property("Color").property("ADBE Color Control-0001");
color_ctrl.setValue(color);
var anchura_ctrl = rect_forma.property("ADBE Effect Parade").property("Ancho").property("ADBE Slider Control-0001");
anchura_ctrl.setValue(w / 2);
var altura_ctrl = rect_forma.property("ADBE Effect Parade").property("Alto").property("ADBE Slider Control-0001");
altura_ctrl.setValue(h / 2);
var grosor_ctrl = rect_forma.property("ADBE Effect Parade").property("Grosor Trazo").property("ADBE Slider Control-0001");
grosor_ctrl.setValue(stroke);

// RECTANGULO VERTICAL

var rect = rect_forma.property("Contents").addProperty("ADBE Vector Group");
rect.name = "Rectángulo Vertical";

var rect_shape = rect.property("Contents").addProperty("ADBE Vector Shape - Rect");
rect_shape.property("ADBE Vector Rect Size").expression = [
														'size_x = effect("Ancho")(1);',
														'size_y = effect("Alto")(1);',
														'[size_x, size_y]',].join("\n");

rect_shape.property("ADBE Vector Shape Direction").setValue(dir);

var rect_col = rect.property("Contents").addProperty("ADBE Vector Graphic - Fill");
rect_col.enabled = false;

var rect_stroke = rect.property("Contents").addProperty("ADBE Vector Graphic - Stroke");
rect_stroke.property("ADBE Vector Stroke Width").setValue(stroke);
rect_stroke.property("ADBE Vector Stroke Color").expression = 'effect("Color")(1)';
rect_stroke.property("ADBE Vector Stroke Width").expression = 'effect("Grosor Trazo")(1)';
rect_stroke.property("ADBE Vector Stroke Line Cap").setValue(3); // Fin de trazo: 1 - Cuadrado; 2 - Redondo; 3 - Proyectante

var rect_trim = rect_forma.property("Contents").addProperty("ADBE Vector Filter - Trim");
rect_trim.name = "Recortar Trazados 1";
var rect_trim_offset = rect_trim.property("ADBE Vector Trim Offset");
var rect_trim_end = rect_trim.property("ADBE Vector Trim End");
rect_trim_end.setValuesAtTimes([rect_forma.inPoint, rect_forma.inPoint + 1], [0, 100]);
rect_trim_end.setTemporalEaseAtKey(2, [easeIn]);

if (dir == 1 || dir == 2) {
	rect_trim_offset.setValue(altura_ctrl.value * 360 / (anchura_ctrl.value * 2 + altura_ctrl.value * 2));
	rect_trim_offset.expression = [
								'var anchura = content("Rectángulo Vertical").content(1).size[0];',
								'var altura = content("Rectángulo Vertical").content(1).size[1]',
								'altura * 360 / (anchura * 2 + altura * 2)',].join("\n");
}

else {
	rect_trim_offset.setValue(anchura_ctrl.value * 360 / (anchura_ctrl.value * 2 + altura_ctrl.value * 2));
	rect_trim_offset.expression = [
								'var anchura = content("Rectángulo Vertical").content(1).size[0];',
								'var altura = content("Rectángulo Vertical").content(1).size[1]',
								'anchura * 360 / (anchura * 2 + altura * 2)',].join("\n");
}


var rect_pos = rect_forma.position;
rect_pos.setValue([w / 2, h / 2])

app.endUndoGroup();