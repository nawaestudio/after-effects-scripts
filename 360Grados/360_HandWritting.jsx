// HAND WRITTING AUTOMATOR
// Guillermo Sevillano 2020

app.beginUndoGroup("HAND");

var comp = app.project.activeItem;
var layer = comp.selectedLayers[0];
var nu = comp.layers.addNull();
nu.name = "HAND WRITE PATH";
var nuMask = nu.Masks.addProperty("Mask");
var nuMaskShape = nuMask.property("maskShape");

vertices = [];
var first_vert_pos = layer.mask(1).maskPath.value.vertices[0];

for (i = 1; i < 10; i++) {
	var mask = layer.mask(i);
	var maskPath = mask.maskPath.value.vertices;
	var n_ofVertices = maskPath.length
	for (j = 0; j < n_ofVertices; j++) {
		cur_vertPair = maskPath[j];
		cur_vertPair = cur_vertPair - first_vert_pos;
		vertices.push(cur_vertPair);
		if (j == n_ofVertices - 1) {
			vertices.push(maskPath[0] - first_vert_pos);
		}
	}
}

nu.transform.position.setValue(first_vert_pos)

var myShape = new Shape();
myShape.vertices = vertices;
myShape.closed = false;

nuMaskShape.setValue(myShape);

var nu_posCtrl = comp.layers.addNull();
nu_posCtrl.name = "HAND WRITE CTRL";
nu_posCtrl.transform.position.setValue(first_vert_pos)

var nu_posCtrl_prop = nu_posCtrl.property("Effects").addProperty("ADBE Slider Control");
nu_posCtrl_prop.name = "Animation";

anim_exp = 'clamp(effect("Animation")(1),0,100)';
nu_posCtrl_prop.expression = anim_exp;

pos_exp = [
		'slider = effect("Animation")(1);',
		'nu = thisComp.layer("HAND WRITE PATH");',
		'nu_pos = nu.transform.position;',
		'l = thisComp.layer("HAND WRITE PATH").mask(1).maskPath.pointOnPath(slider / 100);',
		'[l[0] + nu_pos[0], l[1] + nu_pos[1]]',].join("\n");

nu_posCtrl.transform.position.expression = pos_exp;

app.endUndoGroup();