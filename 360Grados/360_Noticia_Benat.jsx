// NOTICIADOR - 360 GRADOS v1.0
// Guillermo Sevillano 2019

var os = $.os;
var windows = os.indexOf("Windows");

var script_fsName = String(app.project.file.fsName);

if (windows !== -1) { // Si el sistema operativo es Windows
	var script_path = script_fsName.substring(0, script_fsName.lastIndexOf("\\")) + "\\";
}
else {
	var script_path = script_fsName.substring(0, script_fsName.lastIndexOf("/")) + "/";
}

var w = 1920;
var h = 1080;
var dur = 30;
var fps = 25;
var pixelAspect = 1;
var comp_name = "Comp 1";

function aeLog(data) {
	current_time = Date.now();
	log_file.writeln(current_time + ": " + data);
}

function noticiador_Window(thisObj) {

	var w = 450; // Panel Width
	var h = 250; // Panel Height
	var today = new Date();
	var year = today.getFullYear();
	var month = today.getMonth() + 1; // Los meses empiezan en 0
	var day = today.getDate();
	var weekday = ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"]
	var today_weekday = today.getDay();
	var size_multiplicator_slider = 0.5

	var panel_noticia = (thisObj instanceof Panel) ? thisObj : new Window("palette", "360 Noticiador", [0, 0, w, h]);
	
	nt_group = panel_noticia.add("group");
	nt_group.orientation = "column";

	// LINEA 1
	nt_group_row1 = nt_group.add("group");
	nt_group_row1.orientation = "row";

	nt_periodico_sel = nt_group_row1.add("dropdownlist", undefined, ["El País", "El Mundo", "ABC", "El Diario", "El Independiente", "Naiz"]);
	nt_periodico_sel.selection = 0;
	nt_periodico_sel.justify = "left";

	nt_date_weekday_label = nt_group_row1.add("statictext", undefined, "Dia de la semana");
	nt_date_weekday_box = nt_group_row1.add("edittext",undefined, weekday[today_weekday]);
	nt_date_weekday_box.characters = 10;

	nt_date_day_label = nt_group_row1.add("statictext",undefined, "D");
	nt_date_day_box = nt_group_row1.add("edittext",undefined, day);
	nt_date_day_box.characters = 2;

	nt_date_day_box.onChange = function () {
		var n = dayOfTheWeek(parseInt(nt_date_day_box.text), parseInt(nt_date_month_box.text), parseInt(nt_date_year_box.text));
		nt_date_weekday_box.text = weekday[n];
	}

	nt_date_month_label = nt_group_row1.add("statictext",undefined, "M");
	nt_date_month_box = nt_group_row1.add("edittext",undefined, month);
	nt_date_month_box.characters = 2;

	nt_date_month_box.onChange = function () {
		var n = dayOfTheWeek(parseInt(nt_date_day_box.text), parseInt(nt_date_month_box.text), parseInt(nt_date_year_box.text));
		nt_date_weekday_box.text = weekday[n];
	}

	nt_date_year_label = nt_group_row1.add("statictext",undefined, "Y");
	nt_date_year_box = nt_group_row1.add("edittext",undefined, year);
	nt_date_year_box.characters = 4;

	nt_date_year_box.onChange = function () {
		var n = dayOfTheWeek(parseInt(nt_date_day_box.text), parseInt(nt_date_month_box.text), parseInt(nt_date_year_box.text));
		nt_date_weekday_box.text = weekday[n];
	}

	// LINEA 2
	nt_group_row2 = nt_group.add("group");
	nt_group_row2.orientation = "column";
	nt_group_row2.alignment = "left";

		nt_titular_group = nt_group_row2.add("group");
		nt_titular_group.alignment = "left";

			nt_titular_label = nt_titular_group.add("statictext", undefined, "Titular");
			nt_titular_label.alignment = "left"
			nt_titular_box = nt_group_row2.add("edittext",[0,0,w,50],undefined, {multiline:true});
			nt_titular_box.characters = 250;

		nt_group_row2_subgroup1 = nt_group_row2.add("group");
		nt_group_row2_subgroup1.orientation = "row";
		nt_group_row2_subgroup1.alignment = "left";

			nt_tit_only_check = nt_group_row2_subgroup1.add("checkbox", undefined, "Sólo titular");
			nt_tit_only_check.alignment = "left";
			nt_tit_only_check.onClick = function () {
				if (nt_group_row3.visible == true) {
					nt_group_row3.visible = false;
					nt_group_row4.visible = false;
				}

				else {
					nt_group_row3.visible = true;
					nt_group_row4.visible = true;
				}
			}

	// LINEA 3
	nt_group_row3 = nt_group.add("group");
	nt_group_row3.orientation = "column";
	nt_group_row3.alignment = "left";

		nt_parrafo_label = nt_group_row3.add("statictext", undefined, "Párrafo destacado");
		nt_parrafo_label.alignment = "left"
		nt_parrafo_box = nt_group_row3.add("edittext",[0,0,w,150],undefined, {multiline:true});
		nt_parrafo_box.characters = 500;

	// LINEA 4
	nt_group_row4 = nt_group.add("group");
	nt_group_row4.orientation = "column";
	nt_group_row4.alignment = "left",

		nt_ppos_label = nt_group_row4.add("statictext", undefined, "Posición del párrafo");
		nt_ppos_label.alignment = "left";

		nt_group_row4_subgroup1 = nt_group_row4.add("group");
		nt_group_row4_subgroup1.orientation = "row";
		nt_group_row4_subgroup1.alignment = "right";

			nt_ppos_radio1 = nt_group_row4_subgroup1.add("radiobutton", undefined, "Inicio");
			nt_ppos_radio2 = nt_group_row4_subgroup1.add("radiobutton", undefined, "Medio");
			nt_ppos_radio2.value = true;
			nt_ppos_radio3 = nt_group_row4_subgroup1.add("radiobutton", undefined, "Final");

	// LINEA 5

	nt_group_row5 = nt_group.add("group");
	nt_group_row5.orientation = "row";
	nt_group_row5.alignment = "left";

	nt_runScript = nt_group_row5.add("button", undefined, "GO");
	nt_runScript.justify = "left";
	nt_runScript.onClick = function () {
		runNoticiador(panel_noticia);
	}

	nt_sizeMult_label = nt_group_row5.add("statictext", undefined, "Size Multiplicator")
	nt_sizeMult_slider = nt_group_row5.add ('slider {minvalue: 0, maxvalue: 2, value: ' + size_multiplicator_slider +'}');
	nt_sizeMult_slider.size = "width: 200, height: 10";
	nt_sizeMult_slider.onChanging = function () {
		nt_sizeMult_value.text = nt_sizeMult_slider.value;
	}
	nt_sizeMult_value = nt_group_row5.add("statictext", undefined, nt_sizeMult_slider.value);

	panel_noticia.layout.layout(true);
	panel_noticia.minimumSize = panel_noticia.size;
	panel_noticia.layout.resize();
	panel_noticia.onResizing = panel_noticia.onResize = function () {this.layout.resize();}

	panel_noticia.center();
	panel_noticia.show();
}

function dayOfTheWeek (day,month,year) {

	var fecha = new Date(year, month - 1, day);
	var what_weekday = fecha.getDay();

	return what_weekday;
}

function mainCompAnimation(comp, new_comp) {

	comp.openInViewer();
	var news_comp_layer = comp.layers.add(new_comp);
}

function createGrafismo(items, graf_root_name) {

	// Añadir carpeta de GRAFISMO

	var graf_root = items.addFolder(graf_root_name);
	var graf_fld = ["logos", "video", "images"]

	for (i = 0; i < graf_fld.length; i++ ) {
		var cur_graf_fld = items.addFolder(graf_fld[i]);
		cur_graf_fld.parentFolder = graf_root;
	}

	return graf_root;

}

function checkGrafismo() {

	var a = app.project.items;
	var graf_root_name = "00_GRAFISMO";
	var grafismo_exists = false;

	for (i = 1; i <= a.length; i++) {
		if (a[i].name == graf_root_name) {
			grafismo_folder = a[i];
			grafismo_exists = true;
			break;
		}
	}

	if (grafismo_exists == false) {
		var grafismo_folder = createGrafismo(a, graf_root_name);
	}

	return grafismo_folder;
}

function folderCheckAndCreate(comp) {

	// CHECK CARPETA NOTICIAS

	var dls_root_folder = comp.parentFolder;
	var folder_exists = false;
	var news_array = [];

	for (i = 1; i <= dls_root_folder.numItems; i++) { // LOOP de indexado de la carpeta. El rango empieza en 1 en vez de 0
		if (dls_root_folder.item(i) instanceof FolderItem) {
			if (dls_root_folder.item(i).name == "NOTICIAS") {
				folder_exists = true;
				var news_folder = dls_root_folder.item(i);
			}
		}
	}

	if (folder_exists == false) {
		var news_folder = app.project.items.addFolder("NOTICIAS");
			news_folder.parentFolder = comp.parentFolder;
	}
	return news_folder;
}

// CHECK NOTICIAS ANTERIORES

function prevNewsCount(news_folder, nwsppr_sel) {

	var prev_news_count = 1 // Empieza en 1 la cuenta de noticias anteriores del mismo medio

	if (news_folder.numItems != 0){

		for (i = 1; i <= news_folder.numItems; i++) {
			var cur_news_identify = news_folder.item(i).name.split("_");

			if (cur_news_identify[1] == String(nwsppr_sel).toUpperCase()) {
				prev_news_count += 1;
			}
		}
	}

	return prev_news_count;
}

// IMPORTAR LOGOTIPO PERIÓDICO

function logoCheckAndImport(grafismo_folder, nwsppr_path) {

	var logo_array = [];
	var logo_path_split = nwsppr_path.split("/");
	var logo_pathname = logo_path_split[logo_path_split.length - 1];
	var logo_check = false;

	for (l = 1; l <= grafismo_folder.numItems; l++) { // Check si ya está el logo importado al panel de proyecto

		if (grafismo_folder.item(l).name == logo_pathname){
			logo_check = true;
			break;
		}

		if (grafismo_folder.item(l) instanceof FolderItem) {

			var subfolder = grafismo_folder.item(l);

			for (k = 1; k <= subfolder.numItems; k++) {
				if (subfolder.item(k).name == logo_pathname){
					logo_check = true;
					break;
				}
			}
		}
	}

	if (logo_check != true) { // Si el logo no está importado al panel de proyecto

		var io = new ImportOptions(File(nwsppr_path));
		io.importAs = ImportAsType.FOOTAGE;

		var logo_import = app.project.importFile(io);
		logo_import.parentFolder = grafismo_folder.item(2);
	}

	else {

		var logo_found = false;
		for (i = 1; i <= grafismo_folder.numItems; i++) {

			if (logo_found != true) {

				if (grafismo_folder.item(i).name == logo_pathname) {
					logo_found = true;
					logo_import = grafismo_folder.item(i);
					break;
				}

				if (grafismo_folder.item(i) instanceof FolderItem) {

					var subfolder = grafismo_folder.item(i);

					for (k = 1; k <= subfolder.numItems; k++) {
						if (subfolder.item(k).name == logo_pathname) {
							logo_found = true;
							logo_import = subfolder.item(k);
							break;
						}
					}
				}
			}
		}
	}

	return logo_import;
}

function animateNoticia(new_comp, separator, logo, logoMatte, title, parrafo, omit_1, omit_2, weekday, date, title_f_size, parrafo_f_size, weekday_f_size) {

	var frame_dur = 1 / new_comp.frameRate;

	var easeIn = new KeyframeEase(0.5, 100);
	var easeOut = new KeyframeEase(0, 0.1);

	var easeInMid = new KeyframeEase(0, 50);
	var easeOutMid = new KeyframeEase(0, 50);

	// Animación Separador
	var separator_scale = separator.transform.scale;
		separator_scale.setValueAtTime(separator.inPoint, [0,100]);
		separator_scale.setValueAtTime(separator.inPoint + 1, [100,100]);

		separator_scale.setTemporalEaseAtKey(2, [easeIn, easeIn, easeIn], [easeOut, easeOut, easeOut]);

	// Animación titular
	title.inPoint = separator.inPoint + frame_dur * 10;

	var title_anim = title.property("ADBE Text Properties").property("ADBE Text Animators").addProperty("ADBE Text Animator");
		var title_anim_selector = title_anim.property("ADBE Text Selectors").addProperty("ADBE Text Selector");
			var title_anim_selector_shape = title_anim_selector.property("ADBE Text Range Advanced").property("ADBE Text Range Shape");
				title_anim_selector_shape.setValue(2);
			var title_anim_selector_maxease = title_anim_selector.property("ADBE Text Range Advanced").property("ADBE Text Levels Max Ease");
				title_anim_selector_maxease.setValue(-100);
			var title_anim_selector_minease = title_anim_selector.property("ADBE Text Range Advanced").property("ADBE Text Levels Min Ease");
				title_anim_selector_minease.setValue(100);

		var title_anim_opac = title_anim.property("ADBE Text Animator Properties").addProperty("ADBE Text Scale 3D");
			title_anim_opac.setValue([0,0]);

		var title_anim_selector_offset = title_anim_selector.property("ADBE Text Percent Offset");

			title_anim_selector_offset.setValueAtTime(title.inPoint, -100);
			title_anim_selector_offset.setValueAtTime(title.inPoint + 1, 50);

			title_anim_selector_offset.setTemporalEaseAtKey(1, [easeInMid], [easeOutMid]);
			title_anim_selector_offset.setTemporalEaseAtKey(2, [easeInMid], [easeOutMid]);

	if (parrafo !== undefined) { // Si está activado el parrafo destacado

		// Animación parrafo
		parrafo.inPoint = separator.inPoint + 1;
		var s_parrafo = parrafo.sourceRectAtTime(parrafo.inPoint, true);

		var parrafo_pos = parrafo.transform.position;
			parrafo_pos.setValueAtTime(parrafo.inPoint, [parrafo_pos.value[0], parrafo_pos.value[1] - s_parrafo.height / 5]);
			parrafo_pos.setValueAtTime(parrafo.inPoint + 1, [parrafo_pos.value[0], parrafo_pos.value[1] + s_parrafo.height / 5]);
			parrafo_pos.setTemporalEaseAtKey(2, [easeIn], [easeOut]);

		var parrafo_opac = parrafo.transform.opacity;
			parrafo_opac.setValueAtTime(parrafo.inPoint, 0);
			parrafo_opac.setValueAtTime(parrafo.inPoint + 1, 100);
			parrafo_opac.setTemporalEaseAtKey(2, [easeIn], [easeOut]);

		if (omit_1 !== undefined) {
		// Animación omit 1
			omit_1.inPoint = separator.inPoint + 1 + frame_dur * 5;

			var omit_1_anim = omit_1.property("ADBE Text Properties").property("ADBE Text Animators").addProperty("ADBE Text Animator");
			var omit_1_anim_selector = omit_1_anim.property("ADBE Text Selectors").addProperty("ADBE Text Selector");
			var omit_1_anim_opac = omit_1_anim.property("ADBE Text Animator Properties").addProperty("ADBE Text Opacity");
				omit_1_anim_opac.setValue(0);
			var omit_1_anim_pos = omit_1_anim.property("ADBE Text Animator Properties").addProperty("ADBE Text Position 3D");
				omit_1_anim_pos.setValue([-parrafo_f_size, 0]);

				var omit_1_anim_selector_start = omit_1_anim_selector.property("ADBE Text Percent Start");

					omit_1_anim_selector_start.setValueAtTime(omit_1.inPoint, 0);
					omit_1_anim_selector_start.setValueAtTime(omit_1.inPoint + frame_dur * 12, 100);

					omit_1_anim_selector_start.setTemporalEaseAtKey(2, [easeIn], [easeOut]);

			if (omit_2 !== undefined) {
				// Animación omit 2
				omit_2.inPoint = separator.inPoint + 1 + frame_dur * 5;

				var omit_2_anim = omit_2.property("ADBE Text Properties").property("ADBE Text Animators").addProperty("ADBE Text Animator");
				var omit_2_anim_selector = omit_2_anim.property("ADBE Text Selectors").addProperty("ADBE Text Selector");
				var omit_2_anim_opac = omit_2_anim.property("ADBE Text Animator Properties").addProperty("ADBE Text Opacity");
					omit_2_anim_opac.setValue(0);
				var omit_2_anim_pos = omit_2_anim.property("ADBE Text Animator Properties").addProperty("ADBE Text Position 3D");
					omit_2_anim_pos.setValue([-parrafo_f_size, 0]);

					var omit_2_anim_selector_start = omit_2_anim_selector.property("ADBE Text Percent Start");

						omit_2_anim_selector_start.setValueAtTime(omit_2.inPoint, 0);
						omit_2_anim_selector_start.setValueAtTime(omit_2.inPoint + frame_dur * 12, 100);

						omit_2_anim_selector_start.setTemporalEaseAtKey(2, [easeIn], [easeOut]);
			}
		}
	}

	// Animación fecha
	date.inPoint = separator.inPoint + 1 + frame_dur * 7;

	var date_anim = date.property("ADBE Text Properties").property("ADBE Text Animators").addProperty("ADBE Text Animator");
	var date_anim_selector = date_anim.property("ADBE Text Selectors").addProperty("ADBE Text Selector");
	var date_anim_opac = date_anim.property("ADBE Text Animator Properties").addProperty("ADBE Text Opacity");
		date_anim_opac.setValue(0);
	var date_anim_pos = date_anim.property("ADBE Text Animator Properties").addProperty("ADBE Text Position 3D");
		date_anim_pos.setValue([weekday_f_size, 0]);

		var date_anim_selector_end = date_anim_selector.property("ADBE Text Percent End");

			date_anim_selector_end.setValueAtTime(date.inPoint, 100);
			date_anim_selector_end.setValueAtTime(date.inPoint + 1, 0);

			date_anim_selector_end.setTemporalEaseAtKey(2, [easeIn], [easeOut]);

	// Animación dia de la semana
	weekday.inPoint = separator.inPoint + 1 + frame_dur * 12;

	var weekday_anim = weekday.property("ADBE Text Properties").property("ADBE Text Animators").addProperty("ADBE Text Animator");
	var weekday_anim_selector = weekday_anim.property("ADBE Text Selectors").addProperty("ADBE Text Selector");
	var weekday_anim_opac = weekday_anim.property("ADBE Text Animator Properties").addProperty("ADBE Text Opacity");
		weekday_anim_opac.setValue(0);
	var weekday_anim_pos = weekday_anim.property("ADBE Text Animator Properties").addProperty("ADBE Text Position 3D");
		weekday_anim_pos.setValue([weekday_f_size, 0]);

		var weekday_anim_selector_end = weekday_anim_selector.property("ADBE Text Percent End");

			weekday_anim_selector_end.setValueAtTime(weekday.inPoint, 100);
			weekday_anim_selector_end.setValueAtTime(weekday.inPoint + 1, 0);

			weekday_anim_selector_end.setTemporalEaseAtKey(2, [easeIn], [easeOut]);

	// Animación logo
	logoMatte.inPoint = logo.inPoint = separator.inPoint + frame_dur * 7;

	var s_logo = logo.sourceRectAtTime(logo.inPoint, true);

	var logo_pos = logo.transform.position;

		logo_pos.setValueAtTime(logo.inPoint, [logo_pos.value[0], logo_pos.value[1] + s_logo.height * 1.1]);
		logo_pos.setValueAtTime(logo.inPoint + 1, [logo_pos.value[0], logo_pos.value[1] - s_logo.height * 1.1]);

		logo_pos.setTemporalEaseAtKey(2, [easeIn], [easeOut]);

}

function runNoticiador(panel_noticia) {

	app.beginUndoGroup("Title");

	if (app.project.items.length == 0) {
		var comp = app.project.items.addComp(comp_name, w, h, pixelAspect, dur, fps);
		comp.openInViewer();
	}
	else {
		var comp = app.project.activeItem;
	}


	var grafismo_folder = checkGrafismo(); // Comprobar si existe la carpeta de Grafismo

	var nwsppr_sel = nt_periodico_sel.selection;

	var news_folder = folderCheckAndCreate(comp); //CHECK FOLDER AND CREATE

	var prev_news_count = prevNewsCount(news_folder, nwsppr_sel); // CHECK NOTICIAS ANTERIORES MISMO PERIODICO

	new_comp_name = "NOTICIA_" + String(nwsppr_sel).toUpperCase() + "_" + prev_news_count;

	var new_comp = app.project.items.addComp(new_comp_name, w, h, pixelAspect, comp.duration, comp.frameRate);
		new_comp.parentFolder = news_folder;

	mainCompAnimation(comp, new_comp);

	switch (nwsppr_sel.index) {
		case 0: // El País
		var nwsppr_path = script_path + "El_Pais_logo_2007_test.ai";
		break;

		case 1: // El Mundo
		var nwsppr_path = script_path + "El_Mundo_logo.ai";
		break;

		case 2: // ABC
		var nwsppr_path = script_path + "Diario_ABC_logo.ai";
		break;

		case 3: // El Diario
		var nwsppr_path = script_path + "El_Diario_logo.ai";
		break;

		case 4: // El Independiente
		var nwsppr_path = script_path + "El_Independiente.ai";
		break;

		case 5: // Naiz
		var nwsppr_path = script_path + "Naiz_logo.ai";
		break;
	}

	var logo_import = logoCheckAndImport(grafismo_folder, nwsppr_path); // CHECK IF LOGO EXISTS AND IMPORT

	var color = [0.133,0.133,0.133]
	var stroke = 0;

	var size_multiplicator = nt_sizeMult_slider.value;

	var margin = 40 * size_multiplicator;

	// CREAR SEPARADOR

	var separator = new_comp.layers.addShape();
		separator.name = "Separador";
	var separator_rect = separator.property("ADBE Root Vectors Group").addProperty("ADBE Vector Group");
		separator_rect.name = "Rectángulo Vertical";

	var anchura_prop = separator.property("Effects").addProperty("ADBE Slider Control");
		anchura_prop.name = "Grosor Separador";
		var separator_width = anchura_prop.property("ADBE Slider Control-0001");
			separator_width.setValue(10 * size_multiplicator * 1.2);

	var separator_rect_shape = separator_rect.property("ADBE Vectors Group").addProperty("ADBE Vector Shape - Rect");
		separator_rect_shape.name = "Rectangulo 1";
	var separator_rect_size = separator_rect_shape.property("ADBE Vector Rect Size");
		separator_rect_size.setValue([new_comp.width * size_multiplicator, separator_width.value]);

	var separator_col = separator_rect.property("Contents").addProperty("ADBE Vector Graphic - Fill");
		separator_col.property("ADBE Vector Fill Color").setValue(color);
	var separator_stroke = separator_rect.property("Contents").addProperty("ADBE Vector Graphic - Stroke");
		separator_stroke.property("ADBE Vector Stroke Width").setValue(stroke);

	var separator_rect_size_value = separator.property("ADBE Root Vectors Group").property("ADBE Vector Group").property("ADBE Vectors Group").property("ADBE Vector Shape - Rect").property("ADBE Vector Rect Size").value;

	separator.transform.anchorPoint.setValue([-separator_rect_size_value[0] / 2, 0]);

	// CREAR LOGO
	var logo = new_comp.layers.add(logo_import);
		logo.transform.anchorPoint.setValue([0,logo.height]); // Punto de ancla bottom-left
		logo.transform.scale.setValue([logo.transform.scale.value[0] * size_multiplicator, logo.transform.scale.value[1] * size_multiplicator]);
		var s_logo = logo.sourceRectAtTime(logo.inPoint,true);

	// CREAR MATTE LOGO
	var logoMatte = new_comp.layers.addShape();
		logoMatte.moveBefore(logo);
		logoMatte.name = "MATTE_LOGO";
		logoMatte.inPoint = logo.inPoint;
		logoMatte.outPoint = logo.outPoint;

	var matte_margin = logoMatte.property("ADBE Effect Parade").addProperty("ADBE Slider Control");
		matte_margin.name = "Margen Matte";
		matte_margin.property("ADBE Slider Control-0001").setValue(10);

	var matte_rect = logoMatte.property("Contents").addProperty("ADBE Vector Group");
		matte_rect.name = "Rectángulo Matte";

	var matte_shape = matte_rect.property("Contents").addProperty("ADBE Vector Shape - Rect");
	var matte_size = matte_shape.property("ADBE Vector Rect Size");
		matte_size.setValue([s_logo.width * size_multiplicator, s_logo.height * size_multiplicator]);

	var valor = matte_size.value;

	var matte_fill = matte_rect.property("Contents").addProperty("ADBE Vector Graphic - Fill");
		matte_fill.property("ADBE Vector Fill Color").setValue([1,1,1,]);
	var matte_stroke = matte_rect.property("Contents").addProperty("ADBE Vector Graphic - Stroke");
		matte_stroke.property("ADBE Vector Stroke Width").setValue(0);

	if (logo.threeDLayer == false) {
		logoMatte.property("ADBE Transform Group").property("ADBE Rotate Z").setValue(logo.transform.rotation.value);
	}

	else {
		logoMatte.threeDLayer = true; // Si la capa original es 3D, el matte también.
		logoMatte.property("ADBE Transform Group").property("ADBE Rotate Z").setValue(logo.transform.zRotation.value);
	}

	var s_matte = logoMatte.sourceRectAtTime(logoMatte.inPoint,true);
	logoMatte.property("ADBE Transform Group").property("ADBE Anchor Point").setValue([s_matte.left, s_matte.top + s_matte.height]);

	// CREAR TITULAR
	var title_f_size = 100 * size_multiplicator; // Font Size
	var t_char_count = nt_titular_box.text.length;
	var t_char_per_line = separator_rect_size_value[0] / title_f_size * 2;
	var t_line_count = Math.ceil(t_char_count / t_char_per_line); // Math ceiling para evitar que haya cuadros de texto muy pequeños en titulares de menos de una linea
	var t_box_height = t_line_count * title_f_size * 1.6;

	var title = new_comp.layers.addBoxText([separator_rect_size_value[0], t_box_height],nt_titular_box.text);
	var title_prop = title.property("ADBE Text Properties").property("ADBE Text Document");

	var title_text = title_prop.value;
		title_text.fontSize = title_f_size;
		title_text.font = "Merriweather-Bold";
		title_text.fillColor = color;
		title_text.applyFill = true;
		title_text.leading = title_f_size * 1.25;
		title_text.justification = ParagraphJustification.FULL_JUSTIFY_LASTLINE_LEFT;

	title_prop.setValue(title_text);

	var title_pos = title.transform.position.value;

	var s_title = title.sourceRectAtTime(title.inPoint,true); // Tamaño del texto del titular
		title.transform.anchorPoint.setValue([s_title.left,s_title.top]);

	// CREAR PÁRRAFO

	if (nt_tit_only_check.value !== true) { // Si "Solo titular" no está marcado
		var parrafo_f_size = 50 * size_multiplicator; // Font Size
		var p_char_count = nt_parrafo_box.text.length;
		var p_char_per_line = separator_rect_size_value[0] / parrafo_f_size * 2;
		var p_line_count = Math.ceil(p_char_count / p_char_per_line);
		var p_box_height = p_line_count * parrafo_f_size * 1.6;

		var parrafo = new_comp.layers.addBoxText([separator_rect_size_value[0], p_box_height],nt_parrafo_box.text);
		var parrafo_prop = parrafo.property("ADBE Text Properties").property("ADBE Text Document");

		var parrafo_text = parrafo_prop.value;
			parrafo_text.fontSize = parrafo_f_size;
			parrafo_text.font = "Merriweather-Regular";
			parrafo_text.fillColor = color;
			parrafo_text.applyFill = true;
			parrafo_text.leading = parrafo_f_size * 1.25;
			parrafo_text.justification = ParagraphJustification.FULL_JUSTIFY_LASTLINE_LEFT;

		parrafo_prop.setValue(parrafo_text);

		var s_parrafo = parrafo.sourceRectAtTime(parrafo.inPoint,true); // Tamaño del texto del titular
			parrafo.transform.anchorPoint.setValue([s_parrafo.left,s_parrafo.top]);

		// Añadir símbolos de texto anterior/posterior omitido
		if (nt_ppos_radio2.value == true || nt_ppos_radio3.value == true) {
			var omit_1 = new_comp.layers.addText("[...]");
			var omit_1_prop = omit_1.property("ADBE Text Properties").property("ADBE Text Document");

			var omit_1_text = omit_1_prop.value;
				omit_1_text.fontSize = parrafo_f_size;
				omit_1_text.font = "Merriweather-Regular";
				omit_1_text.fillColor = color;
				omit_1_text.applyFill = true;
				omit_1_text.leading = parrafo_f_size * 1.25;
				omit_1_text.justification = ParagraphJustification.FULL_JUSTIFY_LASTLINE_LEFT;

			omit_1_prop.setValue(omit_1_text);

			var omit_1_pos = omit_1.transform.position.value;

			var s_omit_1 = omit_1.sourceRectAtTime(omit_1.inPoint,true); // Tamaño del texto del titular
			omit_1.transform.anchorPoint.setValue([s_omit_1.left,s_omit_1.top]);

			if (nt_ppos_radio2.value == true) {
				var omit_2 = omit_1.duplicate();    // Segundo simbolo de omisión si el párrafo está en medio
			}
		}
	}

	// CREAR FECHA

	var date_f_size = 90 * size_multiplicator;

	var date = new_comp.layers.addText(String(nt_date_day_box.text) + " · " + String(nt_date_month_box.text) + " · " + String(nt_date_year_box.text));
	var date_prop = date.property("ADBE Text Properties").property("ADBE Text Document");

	var date_text = date_prop.value;
		date_text.fontSize = date_f_size;
		date_text.font = "Merriweather-Light";
		date_text.fillColor = color;
		date_text.applyFill = true;
		date_text.justification = ParagraphJustification.RIGHT_JUSTIFY;

	date_prop.setValue(date_text);

	var s_date = date.sourceRectAtTime(date.inPoint,true); // Tamaño del texto del titular

	date.transform.anchorPoint.setValue([s_date.left + s_date.width, s_date.top + s_date.height]);

	// CREAR DIA SEMANA

	var weekday_f_size = 90 * size_multiplicator;

	var weekday = new_comp.layers.addText(nt_date_weekday_box.text);
	var weekday_prop = weekday.property("ADBE Text Properties").property("ADBE Text Document");

	var weekday_text = weekday_prop.value;
		weekday_text.fontSize = weekday_f_size;
		weekday_text.font = "Merriweather-Light";
		weekday_text.fillColor = color;
		weekday_text.applyFill = true;
		weekday_text.justification = ParagraphJustification.RIGHT_JUSTIFY;

	weekday_prop.setValue(weekday_text);

	var s_weekday = weekday.sourceRectAtTime(weekday.inPoint,true); // Tamaño del texto del titular

	weekday.transform.anchorPoint.setValue([s_weekday.left + s_weekday.width, s_weekday.top + s_weekday.height]);

	// REPOSICIONAR TODO

	if (nt_tit_only_check.value !== true) {

		if (nt_ppos_radio2.value == true || nt_ppos_radio3.value == true) {
			var total_height = s_logo.height + s_title.height + s_parrafo.height + margin + separator_width.value + (title_f_size * 0.75) + parrafo_f_size * 3;

			if (nt_ppos_radio2.value == true) {
				total_height += parrafo_f_size * 2;
			}
		}

		else {
			var total_height = s_logo.height + s_title.height + s_parrafo.height + margin + separator_width.value + (title_f_size * 0.75) + parrafo_f_size;
		}
	}

	else {
		var total_height = s_logo.height + s_title.height + margin + separator_width.value;
	}

	separator.transform.position.setValue([(comp.width - separator_rect_size_value[0]) / 2, ((comp.height - total_height) / 2) + s_logo.height]);
	logo.transform.position.setValue([separator.transform.position.value[0], separator.transform.position.value[1] - margin]);
	logoMatte.transform.position.setValue(logo.transform.position.value);
	title.transform.position.setValue([separator.transform.position.value[0], separator.transform.position.value[1] + title_f_size * 0.75]); // Separación entre Logo y titular. Tamaño de letra * interlineado

	if (nt_tit_only_check.value !== true) {

		if (nt_ppos_radio2.value == true || nt_ppos_radio3.value == true) {
			omit_1.transform.position.setValue([separator.transform.position.value[0], title.transform.position.value[1] + s_title.height + parrafo_f_size]);
			parrafo.transform.position.setValue([separator.transform.position.value[0], omit_1.transform.position.value[1] + parrafo_f_size * 2]);

			if (nt_ppos_radio2.value == true) {
				omit_2.transform.position.setValue([omit_1.transform.position.value[0], parrafo.transform.position.value[1] + s_parrafo.height + parrafo_f_size]);
			}
		}

		else {
			parrafo.transform.position.setValue([separator.transform.position.value[0], title.transform.position.value[1] + s_title.height + parrafo_f_size]);
		}

	}

	weekday.transform.position.setValue([separator.transform.position.value[0] + separator_rect_size_value[0], logo.transform.position.value[1] - weekday_f_size]);
	date.transform.position.setValue([separator.transform.position.value[0] + separator_rect_size_value[0], logo.transform.position.value[1]]);

	logo.trackMatteType = TrackMatteType.ALPHA;

	// ANIMACION
	animateNoticia(new_comp, separator, logo, logoMatte, title, parrafo, omit_1, omit_2, weekday, date, title_f_size, parrafo_f_size, weekday_f_size);

	function collapseLayers() {
		for (l = 1; l < new_comp.numLayers; l++) {
			new_comp.layer(l).selected = true;
			app.executeCommand(2771); // Reveal Modified Properties
			app.executeCommand(2771); // Reveal Modified Properties
			new_comp.layer(l).selected = false;
		}
	}

	collapseLayers();

	app.endUndoGroup();

	panel_noticia.close();
}

noticiador_Window(this);