// CREACIÓN DE CORCHETES ANIMADOS
// Guillermo Sevillano 2019

app.beginUndoGroup("Corchete");

var color = [0.133,0.133,0.133,]
var stroke = 0;

var comp = app.project.activeItem;
var corchete_forma = comp.layers.addShape();
corchete_forma.name = "CORCHETE";
corchete_forma.inPoint = comp.time;

// AÑADIR CONTROLES DE EXPRESION

var color_prop = corchete_forma.property("Effects").addProperty("ADBE Color Control");
color_prop.name = "Color Corchete";
var widthorheight_prop = corchete_forma.property("Effects").addProperty("ADBE Checkbox Control");
widthorheight_prop.name = "ANIMATE W/H (Check = Width)";
var animator_prop = corchete_forma.property("Effects").addProperty("ADBE Slider Control");
animator_prop.name = "ANIMATOR";
var anchura_prop = corchete_forma.property("Effects").addProperty("ADBE Slider Control");
anchura_prop.name = "Grosor Corchete";
var longitud_prop = corchete_forma.property("Effects").addProperty("ADBE Slider Control");
longitud_prop.name = "Longitud Corchete";
var longitud_extr_prop = corchete_forma.property("Effects").addProperty("ADBE Slider Control");
longitud_extr_prop.name = "Longitud Extremos";
var reverse_prop = corchete_forma.property("Effects").addProperty("ADBE Checkbox Control");
reverse_prop.name = "Reverse";

// AÑADIR VALUES
var color_ctrl = corchete_forma.property("ADBE Effect Parade").property("Color Corchete").property("ADBE Color Control-0001");
color_ctrl.setValue(color);
var animator_ctrl = corchete_forma.property("ADBE Effect Parade").property("ANIMATOR").property("ADBE Slider Control-0001");
animator_ctrl.expression = 'clamp(effect("ANIMATOR")("Deslizador"),0,100)';
animator_ctrl.setValueAtTime(comp.time, 0); // Animation Start keyframe
animator_ctrl.setValueAtTime(comp.time + 1,100); // Animation End keyframe
var anchura_ctrl = corchete_forma.property("ADBE Effect Parade").property("Grosor Corchete").property("ADBE Slider Control-0001");
anchura_ctrl.setValue(10);
var longitud_ctrl = corchete_forma.property("ADBE Effect Parade").property("Longitud Corchete").property("ADBE Slider Control-0001");
longitud_ctrl.setValue(250);
var longitud_extr_ctrl = corchete_forma.property("ADBE Effect Parade").property("Longitud Extremos").property("ADBE Slider Control-0001");
longitud_extr_ctrl.setValue(25);

// RECTANGULO VERTICAL

var corchete_rect_vert = corchete_forma.property("Contents").addProperty("ADBE Vector Group");
corchete_rect_vert.name = "Rectángulo Vertical";

var rect_vert_size = corchete_rect_vert.property("Contents").addProperty("ADBE Vector Shape - Rect");
rect_vert_size.property("ADBE Vector Rect Size").expression = [
															'size_x = effect("Grosor Corchete")("Deslizador");',
															'size_y = effect("Longitud Corchete")("Deslizador");',
															'[size_x, size_y]',].join("\n");

var corchete_col = corchete_rect_vert.property("Contents").addProperty("ADBE Vector Graphic - Fill");
corchete_col.property("ADBE Vector Fill Color").expression = 'effect("Color Corchete")("Color")';

var corchete_stroke = corchete_rect_vert.property("Contents").addProperty("ADBE Vector Graphic - Stroke");
corchete_stroke.property("ADBE Vector Stroke Width").setValue(stroke);

var corchete_rect_vert_anchor = corchete_rect_vert.property("ADBE Vector Transform Group").property("ADBE Vector Anchor");
corchete_rect_vert_anchor.expression = [
									'anchor_x = content("Rectángulo Vertical").content("Trazado de rectángulo 1").size[0] / 2;',
									'anchor_y = 0;',
									'[-anchor_x, -anchor_y]',].join("\n");

var corchete_rect_vert_scale = corchete_rect_vert.property("ADBE Vector Transform Group").property("ADBE Vector Scale");
corchete_rect_vert_scale.expression = [
									'anim_direction = effect("ANIMATE W/H (Check = Width)")("Casilla de verificación");',
									'if (anim_direction == true) {',
									'scale_x = effect("ANIMATOR")("Deslizador");',
									'scale_y = 100;',
									'}',
									'else {',
									'scale_x = 100;',
									'scale_y = effect("ANIMATOR")("Deslizador");',
									'};',
									'[scale_x,scale_y]',].join("\n");

// EXTREMO SUPERIOR
var corchete_extr_sup = corchete_forma.property("Contents").addProperty("ADBE Vector Group");
corchete_extr_sup.name = "Extremo Superior";

var extr_sup_size = corchete_extr_sup.property("Contents").addProperty("ADBE Vector Shape - Rect");
extr_sup_size.property("ADBE Vector Rect Size").expression = 'bracket_width = effect("Grosor Corchete")("Deslizador");\
															size_x = effect("Longitud Extremos")("Deslizador");\
															size_y = content("Rectángulo Vertical").content("Trazado de rectángulo 1").size[0]; \
															\
															if (size_x < bracket_width) { \
																size_x = bracket_width; \
															} \
															\
															[size_x, size_y];';

var extr_sup_col = corchete_extr_sup.property("Contents").addProperty("ADBE Vector Graphic - Fill");
extr_sup_col.property("ADBE Vector Fill Color").expression = 'effect("Color Corchete")("Color")';

var extr_sup_stroke = corchete_extr_sup.property("Contents").addProperty("ADBE Vector Graphic - Stroke");
extr_sup_stroke.property("ADBE Vector Stroke Width").setValue(stroke);

var extr_sup_anchor = corchete_extr_sup.property("ADBE Vector Transform Group").property("ADBE Vector Anchor");
extr_sup_anchor.expression = 'anchor_x = content("Extremo Superior").content("Trazado de rectángulo 1").size[0] / 2 \
							anchor_y = content("Extremo Superior").content("Trazado de rectángulo 1").size[1] / 2 \
							\
							reversed = effect("Reverse")("Casilla de verificación"); \
							\
							if (reversed == true) { \
								[anchor_x, anchor_y] \
							} \
							\
							else { \
								[-anchor_x, anchor_y] \
							}';

var extr_sup_pos = corchete_extr_sup.property("ADBE Vector Transform Group").property("ADBE Vector Position");
extr_sup_pos.expression =  'relative_scale = content("Rectángulo Vertical").transform.scale[1]; // Escala vertical del corchete \
							main_size = content("Rectángulo Vertical").content("Trazado de rectángulo 1").size;\
							\
							main_scale_x = relative_scale * main_size[0] / 100; \
							main_scale_y = relative_scale * main_size[1] / 100; \
							reversed = effect("Reverse")("Casilla de verificación"); \
							\
							if (reversed == true) { \
								[main_size[0], -main_scale_y / 2] \
							} \
							\
							else { \
								[0, -main_scale_y / 2] \
							}';

// EXTREMO INFERIOR
var corchete_extr_inf = corchete_forma.property("Contents").addProperty("ADBE Vector Group");
corchete_extr_inf.name = "Extremo Inferior";

var extr_inf_size = corchete_extr_inf.property("Contents").addProperty("ADBE Vector Shape - Rect");
extr_inf_size.property("ADBE Vector Rect Size").expression = 'bracket_width = effect("Grosor Corchete")("Deslizador");\
															size_x = effect("Longitud Extremos")("Deslizador");\
															size_y = content("Rectángulo Vertical").content("Trazado de rectángulo 1").size[0]; \
															\
															if (size_x < bracket_width) { \
																size_x = bracket_width; \
															} \
															\
															[size_x, size_y];';

var extr_inf_col = corchete_extr_inf.property("Contents").addProperty("ADBE Vector Graphic - Fill");
extr_inf_col.property("ADBE Vector Fill Color").expression = 'effect("Color Corchete")("Color")';

var extr_inf_stroke = corchete_extr_inf.property("Contents").addProperty("ADBE Vector Graphic - Stroke");
extr_inf_stroke.property("ADBE Vector Stroke Width").setValue(stroke);

var extr_inf_anchor = corchete_extr_inf.property("ADBE Vector Transform Group").property("ADBE Vector Anchor");
extr_inf_anchor.expression = 'anchor_x = content("Extremo Superior").content("Trazado de rectángulo 1").size[0] / 2 \
							anchor_y = content("Extremo Superior").content("Trazado de rectángulo 1").size[1] / 2 \
							\
							reversed = effect("Reverse")("Casilla de verificación"); \
							\
							if (reversed == true) { \
								[anchor_x, -anchor_y] \
							} \
							\
							else { \
								[-anchor_x, -anchor_y] \
							}';

var extr_inf_pos = corchete_extr_inf.property("ADBE Vector Transform Group").property("ADBE Vector Position");
extr_inf_pos.expression =  'relative_scale = content("Rectángulo Vertical").transform.scale[1]; // Escala vertical del corchete \
							main_size = content("Rectángulo Vertical").content("Trazado de rectángulo 1").size;\
							\
							main_scale_x = relative_scale * main_size[0] / 100; \
							main_scale_y = relative_scale * main_size[1] / 100; \
							reversed = effect("Reverse")("Casilla de verificación"); \
							\
							if (reversed == true) { \
								[main_size[0], main_scale_y / 2] \
							} \
							\
							else { \
								[0, main_scale_y / 2] \
							}';

app.endUndoGroup();