// CREACIÓN DE TÍTULOS
// Guillermo Sevillano 2019

app.beginUndoGroup("Titler");

var comp = app.project.activeItem
var sel = comp.selectedLayers;
var sel_name = sel.name;
var time = comp.time;
var capa = sel[0];
var r = capa.sourceRectAtTime(comp.time,true);

// DEFINIR EXPRESIONES

text_anchor_xp = 'capa = thisLayer; \
var r = capa.sourceRectAtTime(capa.inPoint,true); \
var t = r.top; \
var l = r.left; \
var w = r.width; \
var h = r.height; \
[l + w/2, t + h/2]'

text_matte_size_xp = 'i = thisLayer.index; \
var r = thisComp.layer(i + 1).sourceRectAtTime(thisLayer.inPoint,true); \
[r.width, r.height]'

text_anim_pos_xp = 's = thisComp.layer(thisLayer.index - 1).content("Rectángulo Matte").content("Trazado de rectángulo 1").size[1]; \
dir = effect("DIR TOP/BOTTOM (Check = TOP)")("Casilla de verificación"); \
if (dir == true) { \
	[0,s] \
} \
else { [0,-s] }'

capa.transform.anchorPoint.expression = text_anchor_xp;

// AÑADIR FORMA MATTE
var textMatte = comp.layers.addShape();
textMatte.moveBefore(capa);
textMatte.name = "MATTE_TEXTO";
textMatte.inPoint = comp.time;

var matte_rect = textMatte.property("Contents").addProperty("ADBE Vector Group");
matte_rect.name = "Rectángulo Matte";

var matte_size = matte_rect.property("Contents").addProperty("ADBE Vector Shape - Rect");
matte_size.property("ADBE Vector Rect Size").expression = text_matte_size_xp;
var matte_fill = matte_rect.property("Contents").addProperty("ADBE Vector Graphic - Fill");
matte_fill.property("ADBE Vector Fill Color").setValue([1,1,1,]);
var matte_stroke = matte_rect.property("Contents").addProperty("ADBE Vector Graphic - Stroke");
matte_stroke.property("ADBE Vector Stroke Width").setValue(0);

textMatte.property("ADBE Transform Group").property("ADBE Anchor Point").expression = text_anchor_xp;
textMatte.property("ADBE Transform Group").property("ADBE Position").expression = 'i = thisLayer.index; \
thisComp.layer(i + 1).transform.position;'

// AÑADIR EFECTO TEXTO

capa.trackMatteType = TrackMatteType.ALPHA;

var text_anim_when = capa.property("ADBE Effect Parade").addProperty("ADBE Checkbox Control");
text_anim_when.name = "IN/OUT (Check = IN)";
text_anim_when.property("ADBE Checkbox Control-0001").setValue(true);


var text_anim_dir = capa.property("ADBE Effect Parade").addProperty("ADBE Checkbox Control");
text_anim_dir.name = "DIR TOP/BOTTOM (Check = TOP)";
text_anim_dir.enabled = false;

var text_anim = capa.property("ADBE Text Properties").property("ADBE Text Animators").addProperty("ADBE Text Animator");
var text_anim_selector = text_anim.property("ADBE Text Selectors").addProperty("ADBE Text Selector"); // Añade el selector de rango
text_anim_selector.property("ADBE Text Percent Start").setValue(100);
text_anim_selector.property("ADBE Text Percent End").setValueAtTime(comp.time, 0); // Añade el keyframe de entrada al selector final
text_anim_selector.property("ADBE Text Percent End").setValueAtTime(comp.time + 1, 100); // Añade el keyframe de salida al selector final

var text_anim_pos = text_anim.property("ADBE Text Animator Properties").addProperty("ADBE Text Position 3D"); // Añade la propiedad a animar (en este caso la posición)
text_anim_pos.expression = text_anim_pos_xp;

app.endUndoGroup();