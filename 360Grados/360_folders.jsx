// 360 Grados - Creación de carpetas

var bars = "/Volumes/BALEUKO_12/RECURSOS/2_35.png";
var bg = "/Volumes/BALEUKO_12/RECURSOS/BACKGROUNDS_ANIMADOS/PlexusWhite_Prores.mov"

var a = app.project.items;
var graf_root_name = "00_GRAFISMO";

function footageImport(path, parent) {
	var io = new ImportOptions(File(path));
    io.importAs = ImportAsType.FOOTAGE;

    var footage_import = app.project.importFile(io);
    footage_import.parentFolder = parent;
}

function createGrafismo() {

	// Añadir carpeta de GRAFISMO
	var graf_root = a.addFolder(graf_root_name);
	var graf_fld = ["logos", "video", "images"];

	for (i = 0; i < graf_fld.length; i++) {
		var cur_graf_fld = a.addFolder(graf_fld[i]);
		cur_graf_fld.parentFolder = graf_root;
		if (cur_graf_fld.name == "images") {
			var graf_image_fld = cur_graf_fld;
		}
		else if (cur_graf_fld.name == "video") {
			var graf_video_fld = cur_graf_fld;
		}
	}

	footageImport(bars, graf_image_fld);
	footageImport(bg, graf_video_fld);
}


function createDLS(name) {

	if (name != null) {

		// Añadir carpeta raíz DLS
		var dls_root = a.addFolder(name);

		var dls_footage = a.addFolder("FOOTAGE");
		dls_footage.parentFolder = dls_root;

		var dls_comps = a.addFolder("COMPS");
		dls_comps.parentFolder = dls_root;

		var folders = ["audio", "documentos", "images", "grafismo", "screenshots", "video" ];

			for (i = 0; i < folders.length; i++) {
				var cur_folder = a.addFolder(folders[i]);
				cur_folder.parentFolder = dls_footage;
			}
	}
}

function checkGrafismo() {

	var exists = false;

	if (a.length !== 0) {
		for (i = 1; i <= a.length; i++) {
			if (a[i].name == graf_root_name) {
				exists = true;
				break;
			}
		}

		if (exists == false) {
			createGrafismo();
		}

	}

	else {
		createGrafismo();
	}
}

app.beginUndoGroup("Folder Create");

checkGrafismo();
createDLS(prompt("Nombre de la DLS"));

app.endUndoGroup();