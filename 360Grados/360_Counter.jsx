// QUICK COUNTER - 360 GRADOS
// Guillermo Sevillano 2019

var comp = app.project.activeItem;
var time = comp.time;

app.beginUndoGroup("Counter");

var counter_layer = comp.layers.addText();
counter_layer.inPoint = time;
var counter = counter_layer.property("ADBE Effect Parade").addProperty("ADBE Slider Control");
counter.name = "Counter";
var decimals = counter_layer.property("ADBE Effect Parade").addProperty("ADBE Slider Control");
decimals.name = "Decimals";
var separate = counter_layer.property("ADBE Effect Parade").addProperty("ADBE Checkbox Control");
separate.name = "Separate with dots";
var multi = counter_layer.property("ADBE Effect Parade").addProperty("ADBE Slider Control");
multi.name = "Multiplier (n of zeros)";
var pctg = counter_layer.property("ADBE Effect Parade").addProperty("ADBE Checkbox Control");
pctg.name = "Percentage";

src_text_expression = 'dec = effect("Decimals")(1); \
if (dec < 0) { dec = 0 } else if (dec > 100) { dec = 100}; \
\
multi = effect("Multiplier (n of zeros)")(1);\
pctg = effect("Percentage")("ADBE Checkbox Control-0001"); \
\
if (multi < 0) { multi = 0 };\
\
sep = effect("Separate with dots")("ADBE Checkbox Control-0001"); \
n = ((Math.round(effect("ADBE Slider Control")("ADBE Slider Control-0001")  * Math.pow(10,multi)) * Math.pow(10,dec)) / Math.pow(10,dec)).toFixed(dec); \
\
function numberWithCommas(num) { \
    var parts = num.toString().split("."); \
    parts[0] = parts[0].replace(/\\B(?=(\\d{3})+(?!\\d))/g, "."); \
    return parts.join(","); \
} \
\
if (sep == true) { \
	n = numberWithCommas(n); \
} \
else { n = n.toString().replace(".",",") } \
\
if (pctg == true) { \
	n = n + "%"; \
} \
n'; 

var text = counter_layer.property("ADBE Text Properties").property("ADBE Text Document");
text.expression = src_text_expression;

app.endUndoGroup();


