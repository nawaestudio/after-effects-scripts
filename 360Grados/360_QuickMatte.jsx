// 360 QUICK MATTE 1.0
// Creación rápida de mattes para texto o formas.
// Guillermo Sevillano 2019

app.beginUndoGroup("Quick Matte");

var comp = app.project.activeItem
var sel = comp.selectedLayers;
var sel_name = sel.name;
var time = comp.time;
var capa = sel[0];
var name = capa.name;
var r = capa.sourceRectAtTime(capa.inPoint,true);


// DEFINIR EXPRESIONES

anchor_xp = 'capa = thisLayer; \
var r = capa.sourceRectAtTime(capa.inPoint,true); \
var t = r.top; \
var l = r.left; \
var w = r.width; \
var h = r.height; \
[l + w/2, t + h/2]' // Expresión para colocar el punto de anclaje en el centro de la capa

text_matte_size_xp = 'i = thisLayer.index; \
var r = thisComp.layer(i + 1).sourceRectAtTime(thisLayer.inPoint,true); \
var s = thisComp.layer(i + 1).transform.scale; \
var scaleX_ratio = s[0] / 100; \
var scaleY_ratio = s[1] / 100; \
var margin = effect("Margen Matte")("Deslizador"); \
[r.width * scaleX_ratio + margin, r.height * scaleY_ratio + margin]'

text_anim_pos_xp = 's = thisLayer.sourceRectAtTime(thisLayer.inPoint,true); \
[0,s.height]'



// AÑADIR FORMA MATTE
var textMatte = comp.layers.addShape();
textMatte.moveBefore(capa);
textMatte.name = "MATTE_" + name;
textMatte.inPoint = capa.inPoint;
textMatte.outPoint = capa.outPoint;

var matte_margin = textMatte.property("ADBE Effect Parade").addProperty("ADBE Slider Control");
matte_margin.name = "Margen Matte";
matte_margin.property("ADBE Slider Control-0001").setValue(10);

var matte_rect = textMatte.property("Contents").addProperty("ADBE Vector Group");
matte_rect.name = "Rectángulo Matte";

var matte_shape = matte_rect.property("Contents").addProperty("ADBE Vector Shape - Rect");
var matte_size = matte_shape.property("ADBE Vector Rect Size");
matte_size.expression = text_matte_size_xp;
var valor = matte_size.value;

var matte_fill = matte_rect.property("Contents").addProperty("ADBE Vector Graphic - Fill");
matte_fill.property("ADBE Vector Fill Color").setValue([1,1,1,]);
var matte_stroke = matte_rect.property("Contents").addProperty("ADBE Vector Graphic - Stroke");
matte_stroke.property("ADBE Vector Stroke Width").setValue(0);

if (capa.threeDLayer == false) {
	textMatte.property("ADBE Transform Group").property("ADBE Rotate Z").setValue(capa.transform.rotation.value);
}

else {
	textMatte.threeDLayer = true; // Si la capa original es 3D, el matte también.
	textMatte.property("ADBE Transform Group").property("ADBE Rotate Z").setValue(capa.transform.zRotation.value);
}

var matte_r = textMatte.sourceRectAtTime(textMatte.inPoint,true);
textMatte.property("ADBE Transform Group").property("ADBE Anchor Point").setValue([matte_r.left + matte_r.width/2, matte_r.top + matte_r.height/2]);
textMatte.property("ADBE Transform Group").property("ADBE Position").setValue(capa.transform.position.value);

/* var capa_text_animator = capa.property("ADBE Text Properties").property("ADBE Text Animators").addProperty("ADBE Text Animator"); // Animador de texto
var capa_text_animator_selector = capa_text_animator.property("ADBE Text Selectors").addProperty("ADBE Text Selector"); // Seletor de rango
var capa_text_animator_prop = capa_text_animator.property("ADBE Text Animator Properties").addProperty("ADBE Text Position 3D"); // Propiedad a animar */

capa.property("ADBE Transform Group").property("ADBE Anchor Point").setValue([r.left + r.width/2, r.top + r.height/2]);
capa.parent = textMatte;
capa.trackMatteType = TrackMatteType.ALPHA;

/* var text_anim_dir = 1;

if (text_anim_dir == 1) {
	capa_text_animator_prop.expression = text_anim_pos_xp;
} */

app.endUndoGroup();