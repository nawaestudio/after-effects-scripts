// AUTO HIGHLIGHTER - 360 GRADOS
// Guillermo Sevillano 2019

var comp = app.project.activeItem;
var time = comp.time;
var sel = app.project.activeItem.selectedLayers;
var capa = sel[0];

app.beginUndoGroup("Subrayado");

capa.inPoint = time;

var rect = capa.property("ADBE Root Vectors Group").property("ADBE Vector Group");
var high_rect_anchor = rect.property("ADBE Vector Transform Group").property("ADBE Vector Anchor");
var high_rect_pos = rect.property("ADBE Vector Transform Group").property("ADBE Vector Position");
var high_rect_scale = rect.property("ADBE Vector Transform Group").property("ADBE Vector Scale");
var high_rect_col = rect.property("ADBE Vectors Group").property("ADBE Vector Graphic - Fill").property("ADBE Vector Fill Color");
var high_rect_size = rect.property("ADBE Vectors Group").property("ADBE Vector Shape - Rect").property("ADBE Vector Rect Size");


// EXPRESIONES

high_rect_xp_anchor = [
			'x = content("Rectángulo 1").content("Trazado de rectángulo 1").size[0];',
			'y = content("Rectángulo 1").content("Trazado de rectángulo 1").size[1];',
			'[-x/2,-y/2]',].join("\n");

high_rect_xp_pos = [
			'x = content("Rectángulo 1").transform.position[0] + content("Rectángulo 1").transform.anchorPoint[0];',
			'y = content("Rectángulo 1").transform.position[1] + content("Rectángulo 1").transform.anchorPoint[1];',
			'[x,y]',].join("\n");

high_rect_xp_size = [
			'x = effect("Anchura Highlight")("Deslizador");',
			'y = effect("Altura Highlight")("Deslizador");',
			'[x,y]',].join("\n");

high_rect_xp_anim = [
			'x = effect("Animacion (0-1)")("Deslizador")*100;',
			'y = value[1];',
			'[x,y]',].join("\n");

// Set anchor en la esquina izquierda y arriba / Resetea la posición

high_rect_anchor.expression = high_rect_xp_anchor;
high_rect_pos.expression = high_rect_xp_pos;

// Altura de origen para que, después al poner la expresión, no quede a cero

var src_anchura = high_rect_size.value[0];
var src_altura = high_rect_size.value[1];

// Color de origen

var src_color = high_rect_col.value;

// Añade los controles para el subrayado

var high_col_ctrl = capa.property("Effects").addProperty("ADBE Color Control");
high_col_ctrl.name = "Color Highlight";

var high_anchura_ctrl = capa.property("Effects").addProperty("ADBE Slider Control");
high_anchura_ctrl.name = "Anchura Highlight";

var high_altura_ctrl = capa.property("Effects").addProperty("ADBE Slider Control");
high_altura_ctrl.name = "Altura Highlight";

var high_anim_ctrl = capa.property("Effects").addProperty("ADBE Slider Control");
high_anim_ctrl.name = "Animacion (0-1)";

// Añade las expresiones para los controles anteriores

var high_col = capa.property("ADBE Effect Parade").property("Color Highlight").property("ADBE Color Control-0001");
high_col.setValue(src_color);

var high_anchura = capa.property("ADBE Effect Parade").property("Anchura Highlight").property("ADBE Slider Control-0001");
high_anchura.setValue(src_anchura);

var high_altura = capa.property("ADBE Effect Parade").property("Altura Highlight").property("ADBE Slider Control-0001");
high_altura.setValue(src_altura);

var high_anim = capa.property("ADBE Effect Parade").property("Animacion (0-1)").property("ADBE Slider Control-0001");
high_anim.expression = 'clamp(effect("Animacion (0-1)")("Deslizador"),0,1)';
high_anim.setValueAtTime(capa.inPoint,0);
high_anim.setValueAtTime(capa.inPoint + 1, 1);

high_rect_col.expression = 'effect("Color Highlight")("Color")';
high_rect_size.expression = high_rect_xp_size;
high_rect_scale.expression = high_rect_xp_anim;

app.endUndoGroup();